# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>

# ANSI escape codes
RED="\e[1;31m"
GREEN="\e[1;32m"
YELLOW="\e[1;33m"
BLUE="\e[1;34m"
END_COLOUR="\e[0m"


alert() {
  echo -e "\n${RED}[ ALERT ] $1 ${END_COLOUR}\n"
}


checklist() {
  echo -e "\n${YELLOW}[ * ] $1 ${END_COLOUR}\n"
}


message() {
  echo -e "\n${GREEN}$1${END_COLOUR}\n"
}


backup_file() {
  for f in "$@"; do cp "${f}" "${f}.$(date +'%Y-%m-%dT%H%M%S').bak"; done
}


countdown() {
  local message=$1
  local start=$2
  echo -n "$message"
  while [ "${start}" -ge 1 ]
  do
    echo -n "${start}...."
    start=$((start - 1))
    sleep 1
  done
  echo ""
}


verify_efi() {
  local directory="/sys/firmware/efi/efivars"
  if [[ ! -d "$directory" ]]; then
    alert "Script requires UEFI boot: $directory not found."
    exit 1
  fi
}


verify_root() {
  if (( EUID != 0 )); then
    alert "Permission denied. Are you root?"
    exit 1
  fi
}


verify_version() {
  local version="VERSION_ID=\"${VERSION}\""
  local name="VERSION_CODENAME=${CODENAME}"
  local file="/etc/os-release"
  if ! [[ $(grep $version $file) ]] || ! [[ $(grep $name $file) ]]; then
    alert "Script requires $NAME $VERSION."
    exit 1
  fi
}
