#!/usr/bin/env bash
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

set -euo pipefail

SCRIPT=$(basename $0)
SOURCE="https://gitlab.com/dwarmstrong/mintyfresh"
NAME="LMDE"
LONG_NAME="Linux Mint Debian Edition"
VERSION="6"
CODENAME="faye"
PURPOSE="Prepare a disk with encryption for the $NAME \"expert-mode\" installer."

EFI_PART="1"
BOOT_PART="2"
ROOT_PART="3"
HOME_PART="4"
ROOT_DEV="/dev/mapper/root"
HOME_DEV="/dev/mapper/home"


Help() {
  echo "usage: $SCRIPT [-h]"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "options:"
  echo "  -h        show this help message and exit"
}


run_options() {
  while getopts ":h" OPT; do
    case $OPT in
      h)
        Help
        exit
        ;;
      ?)
        echo "\"$OPTARG\": Invalid option"
        exit 1
        ;;
    esac
  done
}


main() {
  local disk="disk"
  local root_part_size=64
  source 1_setup.sh
  source 2_install.sh
  verify_version
  verify_efi
  verify_root
  hello
  set_install_options
  if [[ "$disk" == *"nvme"* ]]; then
    local efi_disk="${disk}p${EFI_PART}"
    local boot_disk="${disk}p${BOOT_PART}"
    local root_disk="${disk}p${ROOT_PART}"
    local home_disk="${disk}p${HOME_PART}"
  else
    local efi_disk="${disk}${EFI_PART}"
    local boot_disk="${disk}${BOOT_PART}"
    local root_disk="${disk}${ROOT_PART}"
    local home_disk="${disk}${HOME_PART}"
  fi
  wipe_disk
  partition_disk
  encrypt_root
  encrypt_home
  create_file_systems
  expert_mode_installer
  mount_device_partition
  script_packages
  install_lmde
  configure_fstab
  configure_crypttab
  finish_install
}


# (O<  Let's go!
# (/)_
run_options "$@"
main
