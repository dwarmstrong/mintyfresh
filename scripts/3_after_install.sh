# Author : Daniel Wayne Armstrong <hello@dwarmstrong.org>


verify_user() {
  if ! [[ -d "/home/${USERNAME}" ]]; then
    err "Home directory for $USERNAME not found."
    exit 1
  fi
}


hello() {
  local script="${BLUE}${SCRIPT}${END_COLOUR}"
  local purpose="configures a fresh install of $LONG_NAME (${NAME} ${VERSION})"
  clear
  echo -e "${GREEN}(O< --${END_COLOUR} Hello! Script $script $purpose."
  echo -e "${GREEN}(/)_${END_COLOUR}"
  echo ""
  echo -e "See '${script} ${BLUE}-h${END_COLOUR}' for options and README for more details."
  echo ""
  echo -e "System will be configured for user ${BLUE}${USERNAME}${END_COLOUR}."
  echo ""

  while :
  do
    read -r -n 1 -p "Proceed? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      countdown "OK....Let's roll in...." 5
      break
    elif [[ "${reply}" == [nN] ]]; then
      echo ""
      echo "Exit."
      exit 0
    else
      echo ""
    fi
  done
}


select_consolefont() {
  local task="Change the font used in the console."
  checklist "$task"
  while :
  do
    read -r -n 1 -p "Select a different console font? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      dpkg-reconfigure console-setup
      break
    elif [[ "${reply}" == [nN] ]]; then
      break
    fi
  done
}


update_system() {
  local task="Update system."
  echo ""
  checklist "$task"
  apt-get update
  apt-get -y dist-upgrade
}


console_packages() {
  local pyenv
  local console
  pyenv="make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev "
  pyenv+="wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev "
  pyenv+="liblzma-dev"
  console="${pyenv} acpi apt-file apt-show-versions apt-utils aptitude command-not-found cbonsai "
  console+="cowsay cryptsetup espeak figlet git gnupg htop imagemagick libxcb-xinerama0 "
  console+="lm-sensors lolcat ncal ncdu newsboat openssh-server plocate rsync shellcheck sl "
  console+="speedtest-cli tealdeer tmux tree unzip vim whois zstd"
  apt-get -y install $console
  apt-file update
}


desktop_packages() {
  local virt
  local desktop
  virt="qemu-system-x86 libvirt-clients libvirt-daemon libvirt-daemon-system virtinst "
  virt+="virt-manager bridge-utils"
  desktop="${virt} dconf-editor ffmpeg fonts-firacode gimp gimp-data-extras gimp-help-en "
  desktop+="libxcb-cursor0 mint-meta-codecs pavucontrol qpdfview rofi vlc"
  apt-get -y install $desktop

  # configure libvirt
  adduser $USERNAME kvm
  adduser $USERNAME libvirt
  adduser $USERNAME libvirt-qemu
  backup_file /etc/libvirt/qemu.conf
  sed -i "s/^#user = \"libvirt-qemu\"/user = \"${USERNAME}\"/" /etc/libvirt/qemu.conf
  systemctl start libvirtd
}


syncthing_package() {
  local keyring="/etc/apt/keyrings/syncthing-archive-keyring.gpg"
  local channel="deb [signed-by=${keyring}] https://apt.syncthing.net/ syncthing stable"
  local pin="Pin: origin apt.syncthing.net"
  # Enable system to check package authenticity by adding the release PGP key:
  curl -L -o ${keyring} https://syncthing.net/release-key.gpg
  # Stable channel is updated with stable release builds, usually every first Tuesday of the month.
  # Add the "stable" channel to APT sources:
  echo "$channel" > /etc/apt/sources.list.d/syncthing.list
  # Ensure system packages do not take preference over those in the syncthing repo by assigning
  # packages in that repo a higher priority ("pinning"):
  printf "Package: *\n${pin}\nPin-Priority: 990\n" > /etc/apt/preferences.d/syncthing.pref
  apt update
  #apt install syncthing
}


boinc_package() {
  local keyring="/etc/apt/keyrings/boinc-archive-keyring.gpg"
  local keyserver="--keyserver keyserver.ubuntu.com"
  local key="--recv-keys 40254C9B29853EA6"
  local repository="https://boinc.berkeley.edu/dl/linux/stable/bookworm bookworm main"
  local pin="Pin: origin boinc.berkeley.edu"
  gpg --homedir /tmp --no-default-keyring --keyring $keyring $keyserver $key
  echo "deb [arch=amd64 signed-by=${keyring}] $repository" > /etc/apt/sources.list.d/boinc.list
  printf "Package: *\n${pin}\nPin-Priority: 990\n" > /etc/apt/preferences.d/boinc.pref
  apt update
  #apt install boinc
}


third_party_packages() {
  boinc_package
  syncthing_package
}


install_packages() {
  console_packages
  desktop_packages
  third_party_packages
  # ... train kept rolling ...
  /usr/games/sl | /usr/games/lolcat || true
}


auto_remove_kernels() {
  local task="Auto-remove obsolete kernels"
  checklist "$task"
  # Remove obsolete kernels and dependencies on a weekly basis. Always leave at least one older 
  # kernel installed and never remove manually installed kernels:
  systemctl enable --now mintupdate-automation-autoremove.timer
  # Create empty placeholder file:
  touch /var/lib/linuxmint/mintupdate-automatic-removals-enabled
}


configure_zram() {
  local task="Configure zram swap"
  local config="/etc/default/zramswap"
  checklist "$task"
  apt-get -y install zram-tools
  swapoff --all
  zramswap stop
  backup_file $config
  sed -i "s/^#ALGO.*/ALGO=zstd/" $config
  sed -i "s/^#PERCENT.*/PERCENT=25/" $config
  sed -i "s/^#PRIORITY.*/PRIORITY=100/" $config
  backup_file /etc/fstab
  sed -e '/swap/ s/^#*/#/' -i /etc/fstab
  zramswap start
}


configure_sudo() {
  local task="Configure sudo"
  checklist "$task"
  echo "${USERNAME} ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/sudoer_${USERNAME}
}


customize_grub() {
  local task="Customize GRUB"
  local theme="mintyfresh"
  local background="${theme}/background.png"
  local font="dejavu-sans-mono-20.pf2"
  local cfg="61_mintyfresh-theme.cfg"
  checklist "$task"
  if ! [[ -d "/boot/grub/themes/${theme}" ]]; then
    cp -av /boot/grub/themes/linuxmint /boot/grub/themes/${theme}
    cp files/grub/${background} /boot/grub/themes/${background}
    cp files/grub/${theme}/theme.txt /boot/grub/themes/${theme}/theme.txt
  fi
  if ! [[ -f "/boot/grub/fonts/${font}" ]]; then
    cp files/grub/${theme}/${font} /boot/grub/fonts/${font}
  fi
  if ! [[ -f "/etc/default/grub.d/${cfg}" ]]; then
    cp files/grub/${theme}/${cfg} /etc/default/grub.d/${cfg}
  fi
  update-grub
}


au_revoir() {
  local message="Done! $NAME is in tip-top form and ready to reboot."
  local info="/etc/mintyfresh_$(date +'%Y-%m-%dT%H%M%S%Z')"
  echo ""
  echo ""
  echo "${message}" | /usr/games/cowsay -W 100 -f tux | /usr/games/lolcat 2>/dev/null
  echo -e "Source: $SOURCE" > $info
}
