# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>


hello() {
  local script="${BLUE}${SCRIPT}${END_COLOUR}"
  local purpose="prepares a disk with encryption for the $NAME \"expert-mode\" installer"
  local howto="HOWTO ${BLUE}[1]${END_COLOUR}"
  local url="https://www.dwarmstrong.org/lmde-install-expert-mode/"
  clear
  echo -e "${GREEN}(O< --${END_COLOUR} Hello! Script $script $purpose."
  echo -e "${GREEN}(/)_${END_COLOUR}"
  echo ""
  echo -e "See '${script} ${BLUE}-h${END_COLOUR}' for options and the $howto for more details."
  echo ""
  echo "[1] \"Install $LONG_NAME in Expert Mode\""
  echo -e "\t${BLUE}<${url}>${END_COLOUR}"
  echo ""
  alert "THIS INSTALL WILL DESTROY ALL EXISTING CONTENT ON TARGET DRIVE!"

  while :
  do
    read -r -n 1 -p "Next step is to set the install options. Proceed? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      break
    elif [[ "${reply}" == [nN] ]]; then
      echo ""
      echo "Exit."
      exit 0
    else
      echo ""
    fi
  done
}


set_install_options() {
  local task="Set install options."
  local query="3"
  local disk_size=0
  checklist "$task"
  while :
  do
    message "## Question 1 of ${query}"
    echo "Disks:"
    echo ""

    # generate array containing disk names for possible install targets
    readarray -t disk_array < \
      <(lsblk --noheadings --nodeps --output NAME | grep --invert-match -e loop -e sr -e zram)
    # display array values
    for disk in "${disk_array[@]}"
    do
      echo "$disk"
    done

    echo ""
    echo "Enter the disk name from the above list where $NAME will be installed"
    while :
    do
      read -r -p "=> " reply
      if [[ -z "$reply" ]]; then
        :
      # verify that specified disk name finds a matching value in disk_array, and retrieve size of
      # disk in GB as an integer
      elif [[ $(echo ${disk_array[@]} | grep --word-regexp $reply) ]]; then
          disk_size="$(lsblk -o NAME,SIZE | grep --max-count=1 $reply | awk 'NR==1 {print $2}' | \
            cut -d '.' -f1 | tr -d 'G')"
          disk="/dev/${reply}"
          break
        else
          echo -e "Disk ${BLUE}${reply}${END_COLOUR} not found."
      fi
    done

    message "## Question 2 of ${query}"
    root_home="${BLUE}root${END_COLOUR} and ${BLUE}home${END_COLOUR}"
    root_default="${BLUE}${root_part_size}${END_COLOUR}"
    echo -e "Total size of ${BLUE}$(echo $disk | cut -d'/' -f3)${END_COLOUR} = ${disk_size}GB"
    echo ""
    echo -e "Separate encrypted partitions for ${root_home} will be created. Root partition size"
    echo -e "is the INTEGER value specified below, and home partition uses the remaining space."
    echo ""
    echo -e "Size of root partition in GB (press ENTER for default: $root_default)"
    while :
    do
      read -r -p "=> " reply
      if [[ -z "${reply}" ]]; then
        if (( $root_part_size > $disk_size )); then
          echo "Size of root partition exceeds total disk space."
        else
          break
        fi
      elif [[ $reply =~ ^[+]?[0-9]+$ ]]; then
        if (( $reply > $disk_size )); then
          echo "Size of root partition exceeds total disk space."
        else
          root_part_size="$reply"
          break
        fi
      else
        echo "Value is not an integer."
      fi
    done

    message "## Question ${query} of ${query}"
    echo -e "Target disk: ${BLUE}$(echo $disk | cut -d'/' -f3) (WIPE DISK)${END_COLOUR}"
    echo -e "Root size  : ${BLUE}${root_part_size}GB${END_COLOUR}"
    echo ""
  
    read -r -n 1 -p "Is this correct? [Yn] or [q]uit => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      countdown "OK....Let's roll in...." 5
      break
    elif [[ "${reply}" == [nN] ]]; then
      echo -e "\nOK....Let's try again....\n\n"
    elif [[ "${reply}" == [qQ] ]]; then
      echo -e "\nExit."
      exit 0
    else
      alert "Invalid reply."
    fi
  done
}


wipe_disk() {
  local task="Wipe $disk"
  checklist "$task"
  wipefs -af $disk
  sgdisk --zap-all --clear $disk
  partprobe $disk
}


partition_disk() {
  local task="Partition $disk"
  checklist "${task}"
  sgdisk -n "${EFI_PART}:1m:+300m" -t "${EFI_PART}:ef00" -c 0:esp $disk
  sgdisk -n "${BOOT_PART}:0:+2g" -t "${BOOT_PART}:8300" -c 0:boot $disk
  sgdisk -n "${ROOT_PART}:0:+${root_part_size}g" -t "${ROOT_PART}:8309" -c 0:root $disk
  sgdisk -n "${HOME_PART}:0:-10m" -t "${HOME_PART}:8309" -c 0:home $disk
  partprobe $disk
}


encrypt_root() {
  local task="Encrypt root partition"
  checklist "$task"
  cryptsetup luksFormat -y --type luks2 $root_disk
  cryptsetup open $root_disk root
}


encrypt_home() {
  local task="Encrypt home partition"
  checklist "$task"
  cryptsetup luksFormat -y --type luks2 $home_disk
  cryptsetup open $home_disk home
}


create_file_systems() {
  local task="Create file systems"
  checklist "$task"
  mkfs.vfat -n ESP $efi_disk
  mkfs.ext4 -L bootfs $boot_disk
  mkfs.ext4 -L rootfs $ROOT_DEV
  mkfs.ext4 -L homefs $HOME_DEV
}


expert_mode_installer() {
  local task="Start the $NAME installer (expert-mode)"
  checklist "$task"
  cat <<EOF
Pause here, and switch to a new tab/window in the terminal.
.
.
.
.
Launch the ${NAME} installer in expert-mode: "sudo live-installer-expert-mode"
.
.
.
.
Proceed as normal up to "Installation Type". Select "Manual Partitioning".

In the "Partitioning" window, click "Expert mode".
.
.
.
.
Then return here to the script.

Before continuing, we mount our target file systems on "/target".

EOF

  while :
  do
    read -r -n 1 -p "Mount filesystems? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      break
    elif [[ "${reply}" == [nN] ]]; then
      echo ""
      echo "Install terminated."
      exit 0
    else
      echo ""
    fi
  done
}


mount_device_partition() {
  local task="Mount encrypted devices and boot partition"
  checklist "$task"
  mount --mkdir LABEL=rootfs /target
  mount --mkdir LABEL=homefs /target/home
  mount --mkdir LABEL=bootfs /target/boot
  mount --mkdir LABEL=ESP /target/boot/efi
}


script_packages() {
  # install applications used later in the script
  apt-get update
  apt-get -y install cowsay lolcat sl
  # ....train kept a rollin'....
  /usr/games/sl | /usr/games/lolcat
}


install_lmde() {
  local task="Install LMDE"
  checklist "$task"
  cat <<EOF
Pause here, and switch back to the installer window and click "Next".
.
.
.
.
Proceed to "Summary" and confirm:

* Home encryption: disabled (entire partition is LUKS-encrypted)
* Install bootloader on $disk
* Use already-mounted /target

When satisfied, click "Install".

${NAME} install proceeds as per usual up to "Installation paused".
.
.
.
.
Then return here to the script.

Before continuing, we configure the "fstab" and "crypttab" files.

EOF

  while :
  do
    read -r -n 1 -p "Configure files? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      break
    elif [[ "${reply}" == [nN] ]]; then
      echo ""
      echo "Install terminated."
      exit 0
    else
      echo ""
    fi
  done
}


configure_fstab() {
  local task="Configure fstab"
  checklist "$task"
  echo "LABEL=ESP     /boot/efi   vfat  defaults  0 1" >> /target/etc/fstab
  echo "LABEL=bootfs  /boot       ext4  defaults  0 1" >> /target/etc/fstab
  echo "LABEL=rootfs  /           ext4  defaults  0 1" >> /target/etc/fstab
  echo "LABEL=homefs  /home       ext4  defaults  0 2" >> /target/etc/fstab
}


configure_crypttab() {
  local task="Configure crypttab"
  checklist "$task"
  echo "root PARTLABEL=root none luks,discard" >> /target/etc/crypttab
  echo "home PARTLABEL=home none luks,discard" >> /target/etc/crypttab
}


au_revoir() {
  local message="Done! $NAME is in tip-top form and ready to reboot."
  local install_info="/target/etc/lmde_install_$(date +'%Y-%m-%dT%H%M%S%Z')"
  echo ""
  echo ""
  echo "${message}" | /usr/games/cowsay -W 100 -f tux | /usr/games/lolcat 2>/dev/null
  touch $install_info
}


clean_up() {
  # /target/boot/efi and /target/boot are auto-unmounted by the installer:
  umount /target/home
  umount -l -n -R /target
  echo "Unmount partitions....Done."
  cryptsetup close home
  cryptsetup close root
  echo "Close cryptdevices....Done."
}


finish_install() {
  local task="Finish install"
  checklist "$task"
  cat <<EOF
Pause here, and switch back to the installer window and click "Next".
.
.
.
.
When prompted:

"Do you want to restart your computer to use the new system?"

....choose "No".
.
.
.
.
Then return here to the script.

EOF

  while :
  do
    read -r -n 1 -p "Unmount partitions and remove encrypted devices? [Yn] => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      au_revoir
      clean_up
      break
    elif [[ "${reply}" == [nN] ]]; then
      au_revoir
      break
    else
      echo ""
    fi
  done
}
