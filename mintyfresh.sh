#!/usr/bin/env bash
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the LICENSE file for more details.

set -euo pipefail

SCRIPT=$(basename $0)
SOURCE="https://gitlab.com/dwarmstrong/mintyfresh"
NAME="LMDE"
LONG_NAME="Linux Mint Debian Edition"
VERSION="6"
CODENAME="faye"
PURPOSE="Configure device after a fresh install of $LONG_NAME ($NAME $VERSION)."
USERNAME="${BASH_ARGV[0]}"
ZRAM="y"


Help() {
  echo "usage: $SCRIPT [-h][-z] USERNAME"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "positional arguments:"
  echo "  USERNAME  primary user"
  echo ""
  echo "options:"
  echo "  -h        show this help message and exit"
  echo "  -z        disable zram creation (default: False)"
}


run_options() {
  while getopts ":hz" OPT; do
    case $OPT in
      h)
        Help
        exit
        ;;
      z)
        ZRAM="n"
        ;;
      ?)
        echo "\"$OPTARG\": Invalid option"
        exit 1
        ;;
    esac
  done
}


main() {
  source scripts/1_setup.sh
  source scripts/3_after_install.sh
  verify_version
  verify_root
  verify_user
  hello
  select_consolefont
  update_system
  install_packages
  auto_remove_kernels
  if [[ "$ZRAM" == "y" ]]; then configure_zram; fi
  #configure_sudo
  customize_grub
  au_revoir
}


# (O<  Let's go!
# (/)_
run_options "$@"
main
